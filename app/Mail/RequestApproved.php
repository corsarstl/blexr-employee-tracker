<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RequestApproved extends Mailable
{
    use Queueable, SerializesModels;

    public $request;

    /**
     * Create a new message instance.
     *
     * @param      $request
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Build the message for request approval.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.requestApproved')
            ->with(['request' => $this->request]);
    }
}
