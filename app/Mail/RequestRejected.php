<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RequestRejected extends Mailable
{
    use Queueable, SerializesModels;

    public $request;

    /**
     * Create a new message instance.
     *
     * @param      $request
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Build the message for request rejection.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.requestRejected')
            ->with(['request' => $this->request]);
    }
}
