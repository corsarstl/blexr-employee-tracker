<?php

namespace App\Http\Controllers;

use App\Helpers\Licenses;
use App\Models\User;
use Illuminate\Http\Request;

class LicenseController extends Controller
{
    /**
     * Get formatted users license data for admin dashboard.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUsersLicenses()
    {
        $licenseTypes = Licenses::getDefaultTypes();
        $userData = [];
        $users = User::with('licenses')->get();

        foreach ($users as $user) {
            $userData[] = $user->getDataWithLicenses();
        }

        return response()->json([
            'users'        => $userData,
            'licenseTypes' => $licenseTypes
        ], 200);
    }

    /**
     * Update license statuses for all users.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateUsersLicenses(Request $request)
    {
        foreach ($request->updatedUsers as $updatedUser) {
            $user = User::find($updatedUser['id']);
            $user->updateLicenses($updatedUser['licenses']);
        }

        return response()->json([], 200);
    }
}
