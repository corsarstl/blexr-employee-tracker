<?php

namespace App\Http\Controllers;

use App\Helpers\RequestsHelper;
use App\Mail\RequestApproved;
use App\Mail\RequestRejected;
use App\Models\Request as WorkFromHomeRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class RequestsController extends Controller
{
    /**
     * Save new work-from-home request to db.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request)
    {
        $newRequest = new WorkFromHomeRequest();
        $newRequest->user_id = auth()->user()->id;
        $newRequest->office_id = $request->officeId;
        $newRequest->is_sick = $request->isSick;
        $newRequest->date_from = $request->timeFrom;
        $newRequest->date_to = $request->timeTo;
        $newRequest->save();

        return response()->json([
            'message' => 'Saved.
            '], 200);
    }

    /**
     * Get a list of requests for administration.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function requestsForAdmin()
    {
        $requests = WorkFromHomeRequest::with(['user', 'office'])->get();
        $formattedRequests = RequestsHelper::formatForAdmin($requests);

        return response()->json([
            'requests' => $formattedRequests
        ], 200);
    }

    /**
     * Get all requests for the authenticated user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userRequests()
    {
        $userId = auth()->user()->id;

        $requests = WorkFromHomeRequest::with(['user', 'office'])
            ->where('user_id', $userId)
            ->get();

        return response()->json([
            'requests' => $requests
        ], 200);
    }

    /**
     * Cancel the request by the authenticated user.
     *
     * @param $requestId
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancel($requestId)
    {
        WorkFromHomeRequest::where('id', $requestId)
            ->update(['status' => 'CANCELLED']);

        return response()->json([], 200);
    }

    /**
     * Get a list of requests according to search params.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function filter(Request $request)
    {
        $requests = RequestsHelper::filterRequests($request);

        return response()->json([
            'requests' => $requests
        ], 200);
    }

    /**
     * Approve selected request by admin.
     *
     * @param $requestId
     * @return \Illuminate\Http\JsonResponse
     */
    public function approve($requestId)
    {
        WorkFromHomeRequest::where('id', $requestId)
            ->update(['status' => 'APPROVED']);

        $updatedRequest = WorkFromHomeRequest::where('id', $requestId)
            ->with(['user', 'office'])
            ->first();

        Mail::to($updatedRequest->user->email)->send(new RequestApproved($updatedRequest));

        return response()->json([
            'id'     => $updatedRequest->id,
            'status' => $updatedRequest->status
        ], 200);
    }

    /**
     * Reject selected request by admin.
     *
     * @param $requestId
     * @return \Illuminate\Http\JsonResponse
     */
    public function reject($requestId)
    {
        WorkFromHomeRequest::where('id', $requestId)
            ->update(['status' => 'REJECTED']);

        $updatedRequest = WorkFromHomeRequest::where('id', $requestId)
            ->with(['user', 'office'])
            ->first();

        Mail::to($updatedRequest->user->email)->send(new RequestRejected($updatedRequest));

        return response()->json([
            'id'     => $updatedRequest->id,
            'status' => $updatedRequest->status
        ], 200);
    }
}
