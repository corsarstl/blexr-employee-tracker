<?php

namespace App\Http\Controllers;

class DashboardController extends Controller
{
    /**
     * Return admin dashboard view after admin successfull login.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function adminDashboad()
    {
        return view('admin.dashboard');
    }

    /**
     * Return user dashboard view after user successfull login.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function userDashboad()
    {
        return view('user.dashboard');
    }
}
