<?php

namespace App\Http\Controllers;

use App\Models\Office;

class OfficeController extends Controller
{
    /**
     * Get list of offices to prepopulate selects in forms.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $offices = Office::select('id', 'city')->get();

        return response()->json([
            'offices' => $offices
        ], 200);
    }
}
