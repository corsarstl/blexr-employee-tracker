<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class SessionController extends Controller
{
    use AuthenticatesUsers;

    /*
     * Show login form for admin.
     */
    public function index()
    {
        return view('signIn');
    }

    /**
     * Sign user or admin in. Return corresponding view.
     *
     * @param LoginRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function signIn(LoginRequest $request)
    {
        if (auth()->attempt([
            'email'    => $request->email,
            'password' => $request->password,
            'is_admin' => 1
        ])) {
            return redirect()->to(route('adminDashboad'));
        } elseif (auth()->attempt([
            'email'    => $request->email,
            'password' => $request->password
        ])) {
            return redirect()->to(route('userDashboad'));
        }
    }

    /**
     * Sign auth user out.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function signOut()
    {
        auth()->logout();

        return redirect()->route('showLogin');
    }
}
