<?php

namespace App\Http\Middleware;

use Closure;

class AdminAuthentication
{
    /**
     * Check, if incoming request is from registered user,
     * and if user is an admin.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check()) {
            if (auth()->user()->is_admin) {
                return $next($request);
            }

            if ($request->wantsJson()) {
                $message = 'You do not have permission to access this resource.';

                return response()->json(['message' => $message], 401);
            }

            return redirect()->back();
        }

        return redirect()->route('showLogin');
    }
}
