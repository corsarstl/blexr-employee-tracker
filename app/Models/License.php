<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class License extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Table that corresponds with current model.
     *
     * @var string
     */
    protected $table = 'licenses';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['type'];

    /**
     * The users that the license is assigned to.
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'license_user', 'license_id', 'user_id');
    }

    /**
     * Get formatted license data for admin dashboard.
     *
     * @return array
     */
    public function formatForAdminDashboard()
    {
        $license = [];

        $license['id'] = $this->id;
        $license['type'] = $this->type;
        $license['isAssigned'] = $this->pivot->is_assigned;

        return $license;
    }
}
