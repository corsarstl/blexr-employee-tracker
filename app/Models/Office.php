<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Office extends Model
{
    /**
     * Table that corresponds with current model.
     *
     * @var string
     */
    protected $table = 'offices';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'state', 'city', 'streetAddress', 'postCode', 'phone'
    ];

    /**
     * The requests for current office.
     */
    public function requests()
    {
        return $this->hasMany('App\Models\Request');
    }
}
