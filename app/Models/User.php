<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Table that corresponds with current model.
     *
     * @var string
     */
    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'first_name', 'last_name'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'is_admin'
    ];

    /**
     * All requests made by current user.
     */
    public function requests()
    {
        return $this->hasMany('App\Models\Request');
    }

    /**
     *  Returns user data with the formatted licenses for frontend.
     *
     * @return array
     */
    public function getDataWithLicenses()
    {
        $userData = [];
        $userData['id'] = $this->id;
        $userData['email'] = $this->email;

        foreach ($this->licenses as $license) {
            $userData['licenses'][] = $license->formatForAdminDashboard();
        }

        return $userData;
    }

    /**
     * Update statuses for user licenses.
     *
     * @param $licenses
     */
    public function updateLicenses($licenses)
    {
        $query = [];

        foreach ($licenses as $license) {
            $query[$license['id']] = [
                'is_assigned' => $license['isAssigned']
            ];
        }

        $this->licenses()->sync($query);
    }

    /**
     * The licenses that are assigned to the user.
     */
    public function licenses()
    {
        return $this->belongsToMany('App\Models\License', 'license_user', 'user_id', 'license_id')
            ->withPivot('is_assigned');
    }
}
