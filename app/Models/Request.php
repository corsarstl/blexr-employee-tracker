<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    /**
     * Table that corresponds with current model.
     *
     * @var string
     */
    protected $table = 'requests';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'office_id', 'is_sick', 'date_from', 'date_to', 'status'
    ];

    /**
     * Office chosen by the user in this request.
     */
    public function office()
    {
        return $this->belongsTo('App\Models\Office');
    }

    /**
     * User, who made current requests.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * Set the request's date from.
     *
     * @param $value
     */
    public function setDateFromAttribute($value)
    {
        $this->attributes['date_from'] = Carbon::createFromFormat('Y-m-d H:i', $value);
    }

    /**
     * Set the request's date to.
     *
     * @param $value
     */
    public function setDateToAttribute($value)
    {
        $this->attributes['date_to'] = Carbon::createFromFormat('Y-m-d H:i', $value);
    }
}
