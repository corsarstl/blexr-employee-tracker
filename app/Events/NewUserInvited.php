<?php

namespace App\Events;

use App\Models\User;
use Illuminate\Queue\SerializesModels;

class NewUserInvited
{
    use SerializesModels;

    public $user;
    public $password;

    /**
     * Create a new event instance.
     *
     * @param User $user
     * @param      $password
     */
    public function __construct(User $user, $password)
    {
        $this->user = $user;
        $this->password = $password;
    }
}
