<?php

namespace App\Listeners;

use App\Events\NewUserInvited;
use App\Mail\Welcome;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendWelcomeEmail implements ShouldQueue
{
    /**
     * Send a welcome email to a new user with the login credentials.
     *
     * @param  NewUserInvited $event
     * @return void
     */
    public function handle(NewUserInvited $event)
    {
        Mail::to($event->user)->send(new Welcome(
            $event->user,
            $event->password
        ));
    }
}
