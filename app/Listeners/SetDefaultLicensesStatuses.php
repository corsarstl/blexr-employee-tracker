<?php

namespace App\Listeners;

use App\Events\NewUserInvited;
use App\Models\License;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;

class SetDefaultLicensesStatuses implements ShouldQueue
{
    /**
     * Set default licenses status for a new user.
     *
     * @param  NewUserInvited $event
     * @return void
     */
    public function handle(NewUserInvited $event)
    {
        $licensesIds = License::pluck('id');

        $event->user;

        foreach ($licensesIds as $licensesId) {
            $event->user->licenses()->attach([
                $licensesId => [
                    'is_assigned' => false,
                    'created_at'  => Carbon::now(),
                    'updated_at'  => Carbon::now()
                ]
            ]);
        }
    }
}
