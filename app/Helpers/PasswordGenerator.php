<?php

namespace App\Helpers;

class PasswordGenerator
{
    public static function generateStrongPass()
    {
        $length = 12;
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $pattern = '/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/';

        do {
            $password = substr(str_shuffle($chars), 0, $length);
        } while (! preg_match($pattern, $password));

        return $password;
    }
}
