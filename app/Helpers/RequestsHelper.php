<?php

namespace App\Helpers;

use App\Models\Request;

class RequestsHelper
{
    /**
     * Format list of requests in proper way.
     *
     * @param $requests
     * @return array
     */
    public static function formatForAdmin($requests)
    {
        $formattedRequests = [];

        foreach ($requests as $request) {
            $formattedRequests[] = [
                'id'             => $request->id,
                'userEmail'      => $request->user->email,
                'isSick'         => $request->is_sick,
                'officeLocation' => $request->office->city,
                'timeFrom'       => $request->date_from,
                'timeTo'         => $request->date_to,
                'createdAt'      => $request->created_at->toDateTimeString(),
                'status'         => $request->status
            ];
        }

        return $formattedRequests;
    }

    /**
     * Get a list of requests according to search params.
     *
     * @param $request
     * @return mixed
     */
    public static function filterRequests($request)
    {
        $query = Request::select('*');

        if ($request->input('searchParams.officeId')) {
            $query = $query->where('office_id', $request->input('searchParams.officeId'));
        }
        if ($request->input('searchParams.isSick') === '0') {
            $query = $query->where('is_sick', $request->input('searchParams.isSick'));
        }
        if ($request->input('searchParams.isSick')) {
            $query = $query->where('is_sick', $request->input('searchParams.isSick'));
        }
        if ($request->input('searchParams.status')) {
            $query = $query->where('status', $request->input('searchParams.status'));
        }

        $requests = $query->latest()->get();

        foreach ($requests as $request) {
            $request['userEmail'] = $request->user->email;
            $request['officeLocation'] = $request->office->city;
            $request['isSick'] = $request->is_sick;
        }

        return $requests;
    }
}
