<?php

namespace App\Helpers;

class Licenses
{
    public static function getDefaultTypes()
    {
        return [
            'email access granted',
            'git repository granted',
            'microsoft office license',
            'trello access granted'
        ];
    }
}
