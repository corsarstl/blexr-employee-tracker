<?php

use App\Models\License;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LicenseUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usersIds = User::pluck('id');
        $licensesIds = License::pluck('id');

        foreach ($usersIds as $userId) {
            foreach ($licensesIds as $licensesId) {
                DB::table('license_user')->insert([
                    'user_id'     => $userId,
                    'license_id'  => $licensesId,
                    'is_assigned' => rand(0, 1),
                    'created_at'  => \Carbon\Carbon::now(),
                    'updated_at'  => \Carbon\Carbon::now()
                ]);
            }
        }
    }
}
