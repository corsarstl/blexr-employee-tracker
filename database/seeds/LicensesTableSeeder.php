<?php

use App\Helpers\Licenses;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LicensesTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $licenses = Licenses::getDefaultTypes();

        foreach ($licenses as $license) {
            DB::table('licenses')->insert([
                'type'       => $license,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
    }
}
