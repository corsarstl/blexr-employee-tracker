<?php

use App\Models\Office;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RequestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usersIds = User::pluck('id')->toArray();
        $officesIds = Office::pluck('id')->toArray();
        $statuses = ['PENDING', 'APPROVED', 'REJECTED', 'CANCELLED'];

        foreach ($usersIds as $userId) {
            DB::table('requests')->insert([
                'user_id'    => $userId,
                'office_id'  => $officesIds[rand(0, count($officesIds) - 1)],
                'is_sick'    => rand(0, 1),
                'date_from'  => Carbon::now()->addDays(2),
                'date_to'    => Carbon::now()->addDays(2)->addHours(4),
                'status'     => $statuses[rand(0, count($statuses) - 1)],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
    }
}
