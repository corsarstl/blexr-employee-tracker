<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LicensesTableSeeder::class);
        factory(App\Models\User::class, 10)->create();
        factory(App\Models\User::class, 1)->states('admin')->create();
        $this->call(LicenseUserTableSeeder::class);
        factory(App\Models\Office::class, 15)->create();
        $this->call(RequestsTableSeeder::class);
    }
}
