<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Office::class, function (Faker $faker) {
    return [
        'state'         => $faker->state,
        'city'          => $faker->unique()->city,
        'streetAddress' => $faker->streetAddress,
        'postCode'      => $faker->postcode,
        'phone'         => $faker->e164PhoneNumber
    ];
});
