<?php

use Faker\Generator as Faker;

$factory->define(App\Models\User::class, function (Faker $faker) {
    return [
        'email'          => $faker->unique()->safeEmail,
        'first_name'     => $faker->firstName,
        'last_name'      => $faker->lastName,
        'password'       => bcrypt('secretN5gr5N'),
        'is_admin'       => 0,
        'remember_token' => str_random(10)
    ];
});

$factory->state(App\Models\User::class, 'admin', [
    'email'    => 'admin@blexr.com',
    'is_admin' => '1',
    'password' => bcrypt('adminS7fl9Hb')
]);
