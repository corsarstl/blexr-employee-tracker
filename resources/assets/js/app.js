
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.Vue = require('vue');
window.moment = require('moment');

import Vue from 'vue';
import flatPickr from 'vue-flatpickr-component';

Vue.component('vue-flatpickr', flatPickr);
Vue.component('users-licenses', require('./components/UsersLicensesComponent'));
Vue.component('new-request', require('./components/NewRequestComponent'));
Vue.component('user-requests', require('./components/UsersRequestsComponent'));
Vue.component('admin-requests-list', require('./components/AdminRequestsListComponent'));

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
const app = new Vue({
    el: '#app',
    data: {
        showAdminLicenses: false,
        showAdminRequests: false,
        showUserRequests: false
    }
});
