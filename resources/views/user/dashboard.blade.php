@extends('layouts.main')

@section('content')
    <div class="container">
        @include('layouts.partials.navbar')

        <div class="row">
            <div class="col-2">
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link active" id="v-pills-requests-tab" data-toggle="pill"
                       href="#v-pills-requests" role="tab" aria-controls="v-pills-requests" aria-selected="true">
                        Requests
                    </a>
                    <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill"
                       href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">
                        Profile
                    </a>
                </div>
            </div>
            <div class="col-10">
                <div class="tab-content" id="v-pills-tabContent">
                    <div class="tab-pane fade show active" id="v-pills-requests" role="tabpanel"
                         aria-labelledby="v-pills-requests-tab">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active"
                                   id="nav-invite-new-user-tab" data-toggle="tab"
                                   href="#nav-invite-new-user" role="tab"
                                   aria-controls="nav-invite-new-user" aria-selected="true"
                                   @click="showUserRequests = ! showUserRequests">
                                    New
                                </a>
                                <a class="nav-item nav-link" id="nav-view-all-tab"
                                   data-toggle="tab" href="#nav-view-all"
                                   role="tab" aria-controls="nav-view-all" aria-selected="false"
                                   @click="showUserRequests = true">
                                    All
                                </a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-invite-new-user" role="tabpanel"
                                 aria-labelledby="nav-invite-new-user-tab">
                                <new-request></new-request>
                            </div>
                            <div class="tab-pane fade" id="nav-view-all" role="tabpanel"
                                 aria-labelledby="nav-view-all-tab">
                                <user-requests v-if="showUserRequests"></user-requests>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade show" id="v-pills-profile" role="tabpanel"
                         aria-labelledby="v-pills-profile-tab">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-invite-change-email-tab" data-toggle="tab"
                                   href="#nav-change-email" role="tab" aria-controls="nav-change-email"
                                   aria-selected="true">
                                    Change email
                                </a>
                                <a class="nav-item nav-link" id="nav-change-pass-tab" data-toggle="tab"
                                   href="#nav-change-pass" role="tab" aria-controls="nav-change-pass" aria-selected="false">
                                    Change pass
                                </a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-change-email" role="tabpanel"
                                 aria-labelledby="nav-change-email-tab">
                                <h2>Change email</h2>
                            </div>
                            <div class="tab-pane fade" id="nav-change-pass" role="tabpanel"
                                 aria-labelledby="nav-change-pass-tab">
                                <h2>Change pass</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection