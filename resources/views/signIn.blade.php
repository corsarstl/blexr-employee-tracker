@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-6 offset-3 mt-5">

                <h2>Welcome to <strong>Blexr</strong>!</h2>

                <form action="{{ route('login') }}" method="post">
                    @csrf

                    <div class="form-group">
                        <label for="email">Email address:</label>
                        <input type="email"
                               class="form-control"
                               value="{{ old('email') }}"
                               name="email"
                               id="email"
                               placeholder="Enter email">
                    </div>

                    @if ($errors->has('email'))
                        <div class="alert alert-danger">
                            {{ $errors->first('email') }}
                        </div>
                    @endif

                    <div class="form-group">
                        <label for="password">Password:</label>
                        <input type="password"
                               class="form-control"
                               name="password"
                               id="password"
                               placeholder="Enter password">
                    </div>

                    @if ($errors->has('password'))
                        <div class="alert alert-danger">
                            {{--TODO fix validation.strong--}}
                            {{ $errors->first('password') }}
                        </div>
                    @endif

                    <button type="submit" class="btn btn-primary">Sign In</button>
                </form>
            </div>
        </div>
    </div>
@endsection