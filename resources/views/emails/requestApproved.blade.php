<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Your request approved!</title>
</head>
<body>
<h1>Hello, {{ $request->user->first_name }}!</h1>

<p>We just wanted to inform you that your work-from-home request created at
    <strong>{{ $request->created_at }}</strong>
    for the office in
    <strong>{{ $request->office->city }}</strong>
    was <strong>approved</strong>.
</p>

<p>Best regards, Blexr Support.</p>
</body>
</html>