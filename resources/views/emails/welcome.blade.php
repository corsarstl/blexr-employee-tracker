<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Welcome</title>
</head>
<body>
    <h1>Welcome to Blexr, {{ $newUser->first_name }}!</h1>

    <p>You were added to the system. Now you can make requests to work from home.</p>
    <p>Please, use the following credentials to enter the system and do not disclose them to any third party:</p>
    <ul>
        <li>Your login: <strong>{{ $newUser->email }}</strong>.</li>
        <li>Your password: <strong>{{ $password }}</strong>.</li>
    </ul>

    <a class="btn btn-primary" href="{{ route('showLogin') }}" role="button">Click here to sign in.</a>

    <p>Best regards, Blexr Support.</p>
</body>
</html>