<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
        <a class="navbar-brand" href="#">Blexr</a>
        <span class="navbar-nav mr-auto mt-2 mt-lg-0">
                    Welcome,
                    <strong>
                        {{ $user->last_name }} {{ $user->first_name }}
                    </strong>
                </span>
        <form class="form-inline my-2 my-lg-0">
            <a href="{{ route('signOut') }}"
               class="btn btn-outline-danger my-2 my-sm-0"
               role="button" aria-pressed="true">
                Sign out
            </a>
        </form>
    </div>
</nav>