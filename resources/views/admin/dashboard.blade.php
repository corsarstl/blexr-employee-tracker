@extends('layouts.main')

@section('content')
    <div class="container">
        @include('layouts.partials.navbar')

        <div class="row">
            <div class="col-2">
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link active" id="v-pills-users-tab" data-toggle="pill"
                       href="#v-pills-users" role="tab" aria-controls="v-pills-users" aria-selected="true">
                        Users
                    </a>
                    <a class="nav-link" id="v-pills-requests-tab" data-toggle="pill"
                       href="#v-pills-requests" role="tab"
                       aria-controls="v-pills-requests" aria-selected="false"
                       @click="showAdminRequests = true">
                        Requests
                    </a>
                </div>
            </div>
            <div class="col-10">
                <div class="tab-content" id="v-pills-tabContent">
                    <div class="tab-pane fade show active" id="v-pills-users"
                         role="tabpanel" aria-labelledby="v-pills-users-tab">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-invite-new-user-tab" data-toggle="tab"
                                   href="#nav-invite-new-user" role="tab" aria-controls="nav-invite-new-user"
                                   aria-selected="true">
                                    Invite new
                                </a>
                                <a class="nav-item nav-link"
                                   id="nav-view-all-tab" data-toggle="tab"
                                   href="#nav-view-all" role="tab"
                                   aria-controls="nav-view-all" aria-selected="false"
                                   @click="showAdminLicenses = true">
                                    Licenses
                                </a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-invite-new-user"
                                 role="tabpanel" aria-labelledby="nav-invite-new-user-tab">
                                @include('admin.inviteNewUser')
                            </div>
                            <div class="tab-pane fade" id="nav-view-all" role="tabpanel"
                                 aria-labelledby="nav-view-all-tab">
                                <users-licenses v-if="showAdminLicenses"></users-licenses>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="v-pills-requests" role="tabpanel"
                         aria-labelledby="v-pills-requests-tab">
                        <admin-requests-list v-if="showAdminRequests"></admin-requests-list>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection