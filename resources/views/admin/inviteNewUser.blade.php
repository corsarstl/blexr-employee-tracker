<div class="jumbotron mt-5">
    <div class="col-12">
        <h1 class="display-4">Hello, master!</h1>

        <hr class="my-4">

        <p class="lead">Here you can invite a new user to the system.</p>
        <p class="lead">Just enter user's email and system will generate a strong password for a new account.</p>
        <p class="lead">An invitation email will be sent as well.</p>
    </div>

    <div class="col-8">
        <form action="{{ route('inviteNewUser') }}" method="post">
            @csrf

            <div class="form-group">
                <label for="email">Email for a new account:</label>
                <input type="email"
                       class="form-control"
                       value="{{ old('email') }}"
                       name="email"
                       id="email"
                       placeholder="Enter email">
            </div>

            @if ($errors->has('email'))
                <div class="alert alert-danger">
                    {{ $errors->first('email') }}
                </div>
            @endif

            <button type="submit" class="btn btn-primary">Invite</button>
        </form>
    </div>
</div>

