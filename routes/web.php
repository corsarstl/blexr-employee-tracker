<?php

Route::redirect('/', '/signin', 301);

// Session
Route::get('/signin', 'Auth\SessionController@index')->name('showLogin');
Route::post('/signin', 'Auth\SessionController@signIn')->name('login');
Route::get('/signout', 'Auth\SessionController@signOut')->name('signOut');

Route::get('/get-offices-list', 'OfficeController@index');

// Admin Dashboard
Route::middleware(['admin'])->group(function () {
    Route::prefix('admin')->group(function () {
        Route::get('/dashboard', 'DashboardController@adminDashboad')->name('adminDashboad');
        Route::post('/invite-new-user', 'UserManagementController@inviteNewUser')->name('inviteNewUser');
        Route::get('/get-users-licenses', 'LicenseController@getUsersLicenses');
        Route::post('/update-users-licenses', 'LicenseController@updateUsersLicenses');
        Route::get('/get-requests', 'RequestsController@requestsForAdmin');
        Route::get('/approve-request/{requestId}', 'RequestsController@approve');
        Route::get('/reject-request/{requestId}', 'RequestsController@reject');
        Route::post('/filter-requests', 'RequestsController@filter');
    });
});

// User Dashboard
Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard', 'DashboardController@userDashboad')->name('userDashboad');
    Route::post('/save-request', 'RequestsController@save');
    Route::get('/get-requests', 'RequestsController@userRequests');
    Route::get('/cancel-request/{requestId}', 'RequestsController@cancel');
});
