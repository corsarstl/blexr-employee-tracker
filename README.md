##Project Setup

1. git clone https://corsarstl@bitbucket.org/corsarstl/blexr-employee-tracker.git
2. composer install
3. npm install
4. php artisan migrate
5. php artisan db:seed

---
Link to login page for users and admin is '/signin'.

Admin credentials:
`login - admin@blexr.com,
pass - adminS7fl9Hb`

User credentials:
`login - any email from 'users' table after seeding,
pass - secretN5gr5N`


Session controller will redirect to corresponding dashboard.
All methods have comments. Bootstap design components are used.
There are some bootstrap tabs and pills that simulate further possible development of the project (CRUD operations for models, pagination, flash messages, etc) just for application to look nicer.
